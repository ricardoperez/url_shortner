Rails.application.routes.draw do
  resources :shortened_urls
  devise_for :users

  root to: 'shortened_urls#index'
  get ':slug', to: 'shortened_urls#redirect', as: :short_url

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
