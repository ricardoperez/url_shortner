require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'create' do
    let(:email) { 'email@example.com' }

    context 'when minimal parameters are correct' do
      subject(:user) { described_class.create(email: email, password: 'password') }

      it do
        expect(user.valid?).to be(true)
      end
    end

    context 'when user with give email already exist' do
      subject(:user) { described_class.create(email: email, password: 'password') }

      before do
        described_class.create(email: email, password: 'password')
      end

      it do
        expect(user.valid?).to be(false)
      end

      it do
        expect(user.errors.full_messages).to eq(['Email has already been taken'])
      end
    end

    context 'when password is missing' do
      subject(:user) { described_class.create(email: email) }

      it do
        expect(user.valid?).to be(false)
      end

      it do
        expect(user.errors.full_messages).to eq(['Password can\'t be blank'])
      end
    end

    context 'when email is empty' do
      subject(:user) { described_class.create(password: 'password') }

      it do
        expect(user.valid?).to be(false)
      end

      it do
        expect(user.errors.full_messages).to eq(['Email can\'t be blank'])
      end
    end
  end
end
