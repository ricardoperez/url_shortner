require 'rails_helper'

RSpec.describe ShortenedUrl, type: :model do
  describe '#create' do
    let(:user) { User.create!(email: 'email@example.com', password: 'password') }
    let(:full_url) { 'https://exampl.com/path/to/something' }

    context 'when all params are correct' do
      subject(:shortened_url) do
        described_class.create(full_url: full_url, user_id: user.id)
      end

      it do
        expect(shortened_url.valid?).to be(true)
      end

      it 'slug has value' do
        expect(shortened_url.slug).not_to be_empty
      end

      it 'counter should be zero' do
        expect(shortened_url.counter).to eq(0)
      end
    end

    context 'when user is missing' do
      subject(:shortened_url) do
        described_class.create(full_url: full_url)
      end

      it do
        expect(shortened_url.valid?).to be(false)
      end

      it do
        expect(shortened_url.errors.full_messages).to eq(['User must exist'])
      end
    end

    context 'when url is incorrect' do
      subject(:shortened_url) do
        described_class.create(full_url: invalid_url, user_id: user.id)
      end

      let(:invalid_url) { 'htt://invalid' }

      it do
        expect(shortened_url.valid?).to be(false)
      end

      it do
        expect(shortened_url.errors.full_messages).to eq(['Full url is not valid.'])
      end
    end
  end
end
