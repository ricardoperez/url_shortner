require 'rails_helper'

RSpec.describe 'shortened_urls/index', type: :view do
  let(:user) { User.create(email: 'email@example.com', password: 'password') }
  let(:full_url) { 'http://example.com' }

  before do
    assign(:shortened_urls, [
             ShortenedUrl.create!(
               full_url: full_url,
               slug: 'Slug',
               user_id: user.id,
               counter: 3
             )
           ])
  end

  it 'renders a list of shortened_urls' do
    render
    assert_select 'tr>td', text: full_url, count: 1
    assert_select 'tr>td', text: 'Slug', count: 1
    assert_select 'tr>td', text: 3.to_s, count: 1
  end
end
