require 'rails_helper'

RSpec.describe 'shortened_urls/new', type: :view do
  before do
    assign(:shortened_url, ShortenedUrl.new(
                             full_url: 'MyString',
                             slug: 'MyString',
                             user_id: 1,
                             counter: 1
                           ))
  end

  it 'renders new shortened_url form' do
    render

    assert_select 'form[action=?][method=?]', shortened_urls_path, 'post' do
      assert_select 'input[name=?]', 'shortened_url[full_url]'
    end
  end
end
