require 'rails_helper'

RSpec.describe 'shortened_urls/edit', type: :view do
  let(:user) { User.create(email: 'email@example.com', password: 'password') }

  before do
    @shortened_url = assign(:shortened_url, ShortenedUrl.create!(
                                              full_url: 'http://example.com',
                                              slug: 'MyString',
                                              user_id: user.id,
                                              counter: 1
                                            ))
  end

  it 'renders the edit shortened_url form' do
    render

    assert_select 'form[action=?][method=?]', shortened_url_path(@shortened_url), 'post' do
      assert_select 'input[name=?]', 'shortened_url[full_url]'
    end
  end
end
