require 'rails_helper'

RSpec.describe 'shortened_urls/show', type: :view do
  let(:user) { User.create(email: 'email@example.com', password: 'password') }

  before do
    @shortened_url = assign(:shortened_url, ShortenedUrl.create!(
                                              full_url: 'http://example.com',
                                              slug: 'Slug',
                                              user_id: user.id,
                                              counter: 3
                                            ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(%r{http://example.com})
    expect(rendered).to match(/Shortened link/)
    expect(rendered).to match(/3/)
  end
end
