FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.com" }
    password { 'password' }
  end

  factory :shortened_url do
    full_url { 'http://example.com/path' }
    sequence(:slug) { |n| "slug#{n}" }
    counter { 0 }

    user
  end
end
