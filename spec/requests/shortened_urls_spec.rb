require 'rails_helper'

RSpec.describe '/shortened_urls', type: :request do
  let(:valid_attributes) do
    { full_url: 'https://theguardian.com/' }
  end

  let(:invalid_attributes) do
    { full_url: 'theguardian' }
  end

  let(:user) { create(:user) }

  before do
    sign_in(user)
  end

  describe 'GET /index' do
    let!(:shortened_url1) { create(:shortened_url, user: user) }
    let!(:shortened_url2) { create(:shortened_url, full_url: 'http://blabla.com') }

    it 'renders user shortened urls' do
      get shortened_urls_url

      expect(response.body).to include(shortened_url1.full_url)
    end

    it 'does not render another user url' do
      get shortened_urls_url

      expect(response.body).not_to include(shortened_url2.full_url)
    end
  end

  describe 'GET /show' do
    context 'when shortened url belongs to current user' do
      let(:shortened_url) { create(:shortened_url, user: user) }

      it 'renders a successful response' do
        get shortened_url_url(shortened_url)

        expect(response).to be_successful
      end
    end

    context 'when shortened url does not belgong to current user' do
      let(:another_user) { create(:user) }
      let(:shortened_url) { create(:shortened_url, user: another_user) }

      it 'renders error message' do
        get shortened_url_url(shortened_url)

        expect(response.body).to include("You don't have access to this resource")
      end
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_shortened_url_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    context 'when shortened url belongs to current user' do
      let(:shortened_url) { create(:shortened_url, user: user) }

      it 'render current user shortened urls' do
        get edit_shortened_url_url(shortened_url)

        expect(response).to be_successful
      end
    end

    context 'when shortened url does not belgong to current user' do
      let(:another_user) { create(:user) }
      let(:shortened_url) { create(:shortened_url, user: another_user) }

      it 'renders error message' do
        get edit_shortened_url_url(shortened_url)

        expect(response.body).to include("You don't have access to this resource")
      end
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new ShortenedUrl' do
        expect do
          post shortened_urls_url, params: { shortened_url: valid_attributes }
        end.to change(ShortenedUrl, :count).by(1)
      end

      it 'redirects to the created shortened_url' do
        post shortened_urls_url, params: { shortened_url: valid_attributes }
        expect(response).to redirect_to(shortened_url_url(ShortenedUrl.last))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new ShortenedUrl' do
        expect do
          post shortened_urls_url, params: { shortened_url: invalid_attributes }
        end.to change(ShortenedUrl, :count).by(0)
      end

      it 'renders error message' do
        post shortened_urls_url, params: { shortened_url: invalid_attributes }

        expect(response.body).to include('Full url is not valid')
      end
    end
  end

  describe 'PATCH /update' do
    let(:shortened_url) { create(:shortened_url, user: user) }

    context 'with valid parameters' do
      let(:new_attributes) do
        { full_url: 'https://bbc.co.uk' }
      end

      it 'updates the requested shortened_url' do
        patch shortened_url_url(shortened_url), params: { shortened_url: new_attributes }
        shortened_url.reload
      end

      it 'redirects to the shortened_url' do
        patch shortened_url_url(shortened_url), params: { shortened_url: new_attributes }
        shortened_url.reload
        expect(response).to redirect_to(shortened_url_url(shortened_url))
      end
    end

    context 'with invalid parameters' do
      it 'renders error message' do
        patch shortened_url_url(shortened_url), params: { shortened_url: invalid_attributes }

        expect(response.body).to include('Full url is not valid')
      end
    end

    context 'when shortened url does not belgong to current user' do
      let(:another_user) { create(:user) }
      let(:shortened_url) { create(:shortened_url, user: another_user) }

      it 'renders error message' do
        patch shortened_url_url(shortened_url)

        expect(response.body).to include("You don't have access to this resource")
      end
    end
  end

  describe 'DELETE /destroy' do
    let!(:shortened_url) { create(:shortened_url, user: user) }

    it 'destroys the requested shortened_url' do
      expect do
        delete shortened_url_url(shortened_url)
      end.to change(ShortenedUrl, :count).by(-1)
    end

    it 'redirects to the shortened_urls list' do
      delete shortened_url_url(shortened_url)
      expect(response).to redirect_to(shortened_urls_url)
    end

    context 'when shortened url does not belgong to current user' do
      let(:another_user) { create(:user) }
      let(:shortened_url) { create(:shortened_url, user: another_user) }

      it 'renders error message' do
        delete shortened_url_url(shortened_url)

        expect(response.body).to include("You don't have access to this resource")
      end
    end
  end
end
