require 'rails_helper'

RSpec.describe '/:slug', type: :request do
  context 'when given slug exists' do
    let(:shortened_url) { create(:shortened_url) }

    it 'redirects to the full url' do
      get short_url_url(shortened_url.slug)

      expect(response).to redirect_to(shortened_url.full_url)
    end
  end

  context 'when given slug does not exist' do
    it 'redirects to the full url' do
      get short_url_url('slug')

      expect(response.body).to include('Page or link not found')
    end
  end
end
