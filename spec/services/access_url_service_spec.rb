require 'rails_helper'

RSpec.describe AccessUrlService do
  subject(:service) { described_class.new(slug) }

  describe '#access' do
    let(:slug) { 'slug' }

    context 'when a shortened_url with a give slug exist' do
      let!(:shortened_url) { create(:shortened_url, slug: slug) }

      it 'returns full url' do
        expect(service.access).to eq(shortened_url.full_url)
      end

      it 'change access counter' do
        service.access
        shortened_url.reload

        expect(shortened_url.counter).to eq(1)
      end
    end

    context 'when a shortened_url with a given slug does not exist ' do
      it do
        expect(service.access).to eq(nil)
      end
    end
  end
end
