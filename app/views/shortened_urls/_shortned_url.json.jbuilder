json.extract! shortened_url, :id, :full_url, :slug, :user_id, :counter, :created_at, :updated_at
json.url shortened_url_url(shortened_url, format: :json)
