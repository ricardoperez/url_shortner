class AccessUrlService
  attr_reader :shortened_url

  def initialize(slug)
    @shortened_url = ShortenedUrl.find_by(slug: slug)
  end

  def self.call(*args)
    new(*args).access
  end

  def access
    return unless shortened_url

    update_counter
    shortened_url.full_url
  end

  private

  def update_counter
    shortened_url.update!(counter: shortened_url.counter + 1)
  end
end
