class ShortenedUrlsController < ApplicationController
  before_action :set_shortened_url, only: %i[show edit update destroy]
  before_action :check_access, only: %i[show edit update destroy]

  before_action :authenticate_user!
  skip_before_action :authenticate_user!, only: %i[redirect]

  def index
    @shortened_urls = ShortenedUrl.where(user: current_user)
  end

  def show; end

  def new
    @shortened_url = ShortenedUrl.new
  end

  def edit; end

  def create
    @shortened_url = ShortenedUrl.new(shortened_url_params)
    @shortened_url.user_id = current_user.id

    if @shortened_url.save
      redirect_to @shortened_url, notice: 'Shortened url was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @shortened_url.update(shortened_url_params)
      redirect_to @shortened_url, notice: 'Shortened url was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @shortened_url.destroy
    redirect_to shortened_urls_url, notice: 'Shortened url was successfully destroyed.'
  end

  def redirect
    full_url = AccessUrlService.call(params[:slug])

    if full_url
      redirect_to(full_url)
    else
      render 'errors/404', status: :not_found
    end
  end

  private

  def check_access
    render 'errors/403', status: :forbidden unless current_user_owner?
  end

  def current_user_owner?
    @shortened_url.user == current_user
  end

  def set_shortened_url
    @shortened_url = ShortenedUrl.find(params[:id])
  end

  def shortened_url_params
    params.require(:shortened_url).permit(:full_url, :slug, :user_id, :counter)
  end
end
