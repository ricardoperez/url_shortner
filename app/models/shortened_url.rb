class ShortenedUrl < ApplicationRecord
  validates :full_url, format: { with: URI::DEFAULT_PARSER.make_regexp(%w[http https]), message: 'is not valid.' }, presence: true
  validates :slug, presence: true, uniqueness: true

  belongs_to :user

  before_validation :generate_slug, on: :create

  private

  def generate_slug
    self.slug = SecureRandom.alphanumeric(10) if slug.blank?

    true
  end
end
