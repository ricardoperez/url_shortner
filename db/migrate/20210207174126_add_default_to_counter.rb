class AddDefaultToCounter < ActiveRecord::Migration[6.1]
  def change
    change_column_default :shortened_urls, :counter, from: nil, to: 0
  end
end
