class AddIndexToShortenedUrl < ActiveRecord::Migration[6.1]
  def change
    add_index(:shortened_urls, :slug, unique: true)
  end
end
