class CreateShortenedUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :shortened_urls do |t|
      t.string :full_url
      t.string :slug
      t.integer :user_id
      t.integer :counter

      t.timestamps
    end
  end
end
