# README

Simple URL shortener implementation in rails.

## Requirements and Installation

#### Ruby version

- 2.7.1

#### Rails version

- 6.1

### Installation, db creation and seeding

    bin/setup

#### Run the server

    bin/rails server

#### How to run the test suite

    bundle exec rspec

#### How to run rubocop and reek

    bundle exec rubocop
    bundle exec reek
